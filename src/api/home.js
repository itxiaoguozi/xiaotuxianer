// 此文件=>定义首页需要的接口函数
import request from '@/utils/request'

/**
 *获取首页头部分类数据
 */
export const findHeadCategory = () => {
  return request('/home/category/head', 'get')
}

/**
 *获取品牌
 * @param {Number} limit 获取几个品牌
 */
export const findBrand = (limit) => {
  return request('/home/brand', 'get', { limit })
}
/**
 * 获取轮播图数据
 */
export const findBanner = () => {
  return request('/home/banner', 'get')
}

/**
 * 获取新鲜好物数据
 */
export const findNew = () => {
  return request('/home/new', 'get')
}

/**
 * 获取人气好物数据
 */
export const findHot = () => {
  return request('/home/hot', 'get')
}

/**
 * 获取产品区块数据
 */
export const findProduct = () => {
  return request('/home/goods', 'get')
}

/**
 * 获取最新专题
 */
export const findSpecial = () => {
  return request('/home/special', 'get')
}
