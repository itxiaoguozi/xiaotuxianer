// 其实就是vue插件, 扩展vue功能, 全局组件,指令,函数(vue3.0取消过滤器)
// 挨个导入
// import XtxSkeleton from './xtx-skeleton'
// // 当你在main.js导入,使用Vue.use()(vue3.0 app)的时候就会执行install函数
// import XtxCarousel from './xtx-carousel'
// import XtxMore from './xtx-more'
// import XtxBread from './xtx-bread'
// import XtxBreadItem from './xtx-bread-item'

// 导入library文件夹下的所有组件
// 批量导入需要使用一个函数 require.context(dir,deep,regular)
// 参数: 1.目录 2.是否加载子目录 3.加载的正则匹配
const importFn = require.context('./', false, /\.vue$/)
// console.log(importFn.keys()) 文件名称数组

export default {
  install (app) {
    // 挨个注册
    // app.component(XtxSkeleton.name, XtxSkeleton)
    // app.component(XtxCarousel.name, XtxCarousel)
    // app.component(XtxMore.name, XtxMore)
    // app.component(XtxBread.name, XtxBread)
    // app.component(XtxBreadItem.name, XtxBreadItem)

    // 批量注册全局组件
    importFn.keys().forEach(key => {
      // 导入组件
      const component = importFn(key).default
      // console.log(component)  这是组件
      // 注册组件
      app.component(component.name, component)
    })
    // 定义指令
    defineDirective(app)
  }
}
const defineDirective = (app) => {
  // 专门定义自定义vue指令
  // 图片懒加载指令 v-lazyload
  app.directive('lazyload', {
    // 渲染dom后,vue2.0 使用inserted vue3.0使用mounted
    mounted (el, binding) {
      // el是图片元素,binding.value是图片地址
      // console.log(el)
      // 当图片元素进入可视区域后,将binding.value赋值给el.src即可
      const observe = new IntersectionObserver(([{ isIntersecting }]) => {
        if (isIntersecting) {
          el.src = binding.value
          // 停止观察
          observe.unobserve(el)
        }
      }, {
        threshold: 0.01
      })
      // 开始观察
      observe.observe(el)
    }
  })
}
