import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 导入重置样式库
import 'normalize.css'
// 引入网站公用样式
import '@/assets/styles/common.less'
// mock数据
import '@/mock'

// 自己的插件
import plugin from '@/components/library'

// 创建一个vue应用实例
createApp(App).use(store).use(router).use(plugin).mount('#app')
