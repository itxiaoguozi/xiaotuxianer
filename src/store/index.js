import { createStore } from 'vuex'
// 引入用户模块
import user from './modules/user'
// 引入购物车模块
import cart from './modules/cart'
// 导入插件
import createPersistedstate from 'vuex-persistedstate'
// 导入分类模块
import category from './modules/category'

// 创建vuex仓库并导出
export default createStore({
  state: {
    // 数据
  },
  mutations: {
    // 改数据
  },
  actions: {
    // 请求数据函数(异步)
  },
  modules: {
    // 分模块
    user,
    cart,
    category
  },
  getters: {
    // vuex的计算属性
  },
  plugins: [
    // 默认存储到localstorage
    // 优先从本地读取,写入后才会修改本地数据
    createPersistedstate({
      // 本地存储中的键名
      key: 'erabbit-client-pc-store',
      // 默认是所有的数据都保存,加上这个path,只要user和cart里的数据存储到本地
      paths: ['user', 'cart']
    })
  ]
})
