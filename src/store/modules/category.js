// 存储的分类数据
import { topCategory } from '@/api/constants'
// 引入请求方法
import { findHeadCategory } from '@/api/home'

export default {
  namespaced: true,
  state: () => {
    return {
      // 如果默认是空数组,看不见默认的9个分类,等你数据加载完毕才会看到.
      // 所以:根据常量数据来生成一个默认的顶级分类数据,不会出现空白(没数据的情况)
      // ({ name: item }))=>不写()默认是方法体,加上()相当是对象
      list: topCategory.map(item => ({ name: item }))
    }
  },
  // 加载数据成功后需要修改list所以需要mutations函数
  mutations: {
    setCategory (state, headCategory) {
      state.list = headCategory
    },
    // 修改当前一级分类下的open数据为true
    show (state, item) {
      const category = state.list.find(catehory => catehory.id === item.id)
      category.open = true
    },
    // 修改当前一级分类下的open数据为false
    hide (state, item) {
      const category = state.list.find(catehory => catehory.id === item.id)
      category.open = false
    }
  },
  // 需要向后台加载数据
  actions: {
    async getCategory ({ commit }) {
      const { result } = await findHeadCategory()
      // console.log(result)  自己本地的数据,打印看到
      // 给一级分类加上一个控制二级分类显示隐藏的数据open
      result.forEach(item => {
        // 默认二级分类不显示
        item.open = false
      })
      // 获取数据成功,提交mutations进行数据的修改
      commit('setCategory', result)
    }
  }
}
