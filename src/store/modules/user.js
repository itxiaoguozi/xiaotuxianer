// 管理用户信息的模块

export default {
  namespaced: true,
  state: () => {
    return {
      // 用户信息
      profile: {
        id: '',
        nickname: '',
        avatar: '',
        mobile: '',
        token: ''
      }
    }
  },
  mutations: {
    // 将来登录成功,传入用户信息对象
    /**
     *
     * @param {*} state  state对象
     * @param {*} profile 传入的用户信息
     */
    setUser (state, profile) {
      state.profile = profile
    }
  }
}
