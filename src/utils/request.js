// 1.初始化axios实例
// 2,请求拦截器:带token
// 3.响应拦截器,拿出响应数据,拦截token失效
// 4.定义一个函数使用配置好的axios发请求
// 得到:请求工具函数
import axios from 'axios'
// 引入store
import store from '@/store'
// 导入路由
import router from '@/router/index'

const instance = axios.create({
  baseURL: 'http://pcapi-xiaotuxian-front.itheima.net/',
  // 请求超时
  timeout: 5000
})

// 请求拦截器=>两个参数(请求成功/请求失败)
instance.interceptors.request.use((config) => {
  // config 是请求配置
  // 1.获取token
  const { token } = store.state.user.profile
  // 2.请求头设置token
  if (token) config.headers.Authorization = `Bearer ${token}`
  return config
}, e => Promise.reject(e))

// 响应拦截器
instance.interceptors.response.use(res => res.data, e => {
  // 当token未传,已失效,响应状态码401
  if (e.response && e.response.status === 401) {
    // 获取当前的路由地址(完整地址 哈希地址+?传参) #后的地址
    // router===$router   路径 $router.fullpath===>router.currentRoute.fullpath
    // vue官网上有对应的说明
    // 跳转时候=>完整地址会有问题,有?等字符
    // 所以,跳转前对地址进行URL编码
    // router.currentRoute(响应式数据)是ref里定义的,需要.value拿到
    console.log(router.currentRoute)
    const redirectUrl = encodeURIComponent(router.currentRoute.value.fullpath)
    // 跳转登录页面,并且传递当前路由地址(目的:将来登录完毕后可以回跳)
    router.push({ path: '/login', query: { redirectUrl } })
  }
  return Promise.reject(e)
})

/**
 * @param {string} url 请求地址
 * @param {string} method 请求类型
 * @param {Object} submitData 对象类型,提交数据
 */
export default (url, method, submitData) => {
  // 使用配置好的axios来发请求
  // 返回axios的调用,返回的就是一个promise
  return instance({
    url,
    method,
    // params get方式传参  data 其他方式(请求体)
    // params: submitData,
    // data: submitData
    [method.toLowerCase() === 'get' ? 'params' : 'data']: submitData
  })
}
